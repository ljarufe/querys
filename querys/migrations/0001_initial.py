# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Blog'
        db.create_table(u'querys_blog', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('tagline', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'querys', ['Blog'])

        # Adding model 'Author'
        db.create_table(u'querys_author', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
        ))
        db.send_create_signal(u'querys', ['Author'])

        # Adding model 'Entry'
        db.create_table(u'querys_entry', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('blog', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['querys.Blog'])),
            ('headline', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('body_text', self.gf('django.db.models.fields.TextField')()),
            ('pub_date', self.gf('django.db.models.fields.DateField')()),
            ('mod_date', self.gf('django.db.models.fields.DateField')()),
            ('n_comments', self.gf('django.db.models.fields.IntegerField')()),
            ('n_pingbacks', self.gf('django.db.models.fields.IntegerField')()),
            ('rating', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'querys', ['Entry'])

        # Adding M2M table for field authors on 'Entry'
        m2m_table_name = db.shorten_name(u'querys_entry_authors')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('entry', models.ForeignKey(orm[u'querys.entry'], null=False)),
            ('author', models.ForeignKey(orm[u'querys.author'], null=False))
        ))
        db.create_unique(m2m_table_name, ['entry_id', 'author_id'])


    def backwards(self, orm):
        # Deleting model 'Blog'
        db.delete_table(u'querys_blog')

        # Deleting model 'Author'
        db.delete_table(u'querys_author')

        # Deleting model 'Entry'
        db.delete_table(u'querys_entry')

        # Removing M2M table for field authors on 'Entry'
        db.delete_table(db.shorten_name(u'querys_entry_authors'))


    models = {
        u'querys.author': {
            'Meta': {'object_name': 'Author'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'querys.blog': {
            'Meta': {'object_name': 'Blog'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'tagline': ('django.db.models.fields.TextField', [], {})
        },
        u'querys.entry': {
            'Meta': {'object_name': 'Entry'},
            'authors': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['querys.Author']", 'symmetrical': 'False'}),
            'blog': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['querys.Blog']"}),
            'body_text': ('django.db.models.fields.TextField', [], {}),
            'headline': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mod_date': ('django.db.models.fields.DateField', [], {}),
            'n_comments': ('django.db.models.fields.IntegerField', [], {}),
            'n_pingbacks': ('django.db.models.fields.IntegerField', [], {}),
            'pub_date': ('django.db.models.fields.DateField', [], {}),
            'rating': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['querys']