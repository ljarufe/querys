# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Entry.rating'
        db.alter_column(u'querys_entry', 'rating', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'Entry.mod_date'
        db.alter_column(u'querys_entry', 'mod_date', self.gf('django.db.models.fields.DateField')(null=True))

    def backwards(self, orm):

        # Changing field 'Entry.rating'
        db.alter_column(u'querys_entry', 'rating', self.gf('django.db.models.fields.IntegerField')(default=0))

        # Changing field 'Entry.mod_date'
        db.alter_column(u'querys_entry', 'mod_date', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 10, 29, 0, 0)))

    models = {
        u'querys.author': {
            'Meta': {'object_name': 'Author'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'querys.blog': {
            'Meta': {'object_name': 'Blog'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'tagline': ('django.db.models.fields.TextField', [], {})
        },
        u'querys.entry': {
            'Meta': {'object_name': 'Entry'},
            'authors': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['querys.Author']", 'symmetrical': 'False', 'blank': 'True'}),
            'blog': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['querys.Blog']"}),
            'body_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'headline': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mod_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'n_comments': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'n_pingbacks': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'pub_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'rating': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['querys']