from django.db import models
import datetime


class Blog(models.Model):
    name = models.CharField(max_length=100)
    tagline = models.TextField()

    def __str__(self):
        return u"%s" % self.name


class Author(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()

    def __str__(self):
        return u"%s" % self.name


class Entry(models.Model):
    blog = models.ForeignKey(Blog)
    headline = models.CharField(max_length=255)
    body_text = models.TextField(blank=True)
    pub_date = models.DateField(default=datetime.datetime.now)
    mod_date = models.DateField(null=True, blank=True)
    authors = models.ManyToManyField(Author, blank=True)
    n_comments = models.IntegerField(default=0)
    n_pingbacks = models.IntegerField(default=0)
    rating = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return u"%s" % self.headline